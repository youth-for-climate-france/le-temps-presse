Le Temps Presse
===============

Un journal en ligne de jeunes de Youth For Climate France.

## Introduction

Ce site a été réalisé en [Hugo](https://gohugo.io). Cette bibliothèque web légère permet de générer un site statique facilement.

### Pour toute modification du site

Suivez le tutorial du site de Hugo pour installer **hugo-extended** (pas la version de base, pour la compliation des styles SCSS).

Vous pouvez alors lancer dans une invite de commande (sans le $, depuis le dossier racine du projet)

    $ hugo server -D

Ce qui vous permettra de modifier le site et de voir en direct vos changements en allant à l'adresse <http://localhost:1313/>.

### Pour mettre en ligne le site

Pour la mise en ligne, commencez par générer tous les fichiers finaux, en tapant la commande :

    $ hugo

Tous les fichiers sont alors mis dans `public/`. Vous pouvez éventuellement nettoyer le sitemap de toutes les pages autres que `/`, `/numero/1/', etc.


## Pour développer le site

Les templates sont dans `layout`, il n'y a pas de thèmes. Pour le reste, référez-vous à la documentation de Hugo.


## Pour mettre en page un numéro

### Initialisation

Pour commencer, créez un nouveau numéro (par exemple 12) en tapant dans une invite de commandes :

    $ hugo new numero/12/_index.md

Puis modifiez le fichier créé `content/numero/12/_index.md` en ajoutant à la main, entre les deux "---" :

    dateFR: "Juillet 2042"
    type: journal
    menu: main

et en modifiant `title` pour être "Numéro 12" (ce sont des paramètres nécessaires pour les menus).

Créez également un dossier `static/numero/12/` qui contiendra les fichiers liés (images, pdf...).

### Ajouter un article

Pour créer un nouvel article, tapez la commande :

    $ hugo new numero/12/monarticle.md

Puis modifiez le fichier créé `content/numero/12/monarticle.md` en ajoutant entre les deux "---" :

    order: [n° de l'article dans le journal]

et en modifiant `title`.

Protip : pour insérer un article entre le 4 et le 5 sans tout décaler, vous pouvez mettre nombres flottants, comme 4.5 (notation américaine avec .

Vous pouvez ensuite rédiger votre article en dessous des "---", en utilisant la syntaxe markdown. Sans rentrer dans les détails, utilisez ## en début de ligne pour mettre un titre, \*abc\* pour mettre en italique, \*\*abs\*\* pour mettre en gras, etc. Sautez deux lignes pour passer d'un paragraphe à un autre.

Pour insérer des images, utilisez la syntaxe spéciale de hugo :

    {{< figure src="monimage.png" width="70%" >}}

(en remplaçant `monimage.png` et `70%` par les valeurs qui vous conviennent). Le fichier `monimage.png` doit se trouver dans le dossier `static` sous la même arborescence que la page (i.e. `static/numero/12/`)

### Autres éléments

La version pdf du numéro **doit** s'appeler `letempspresse.pdf` et être dans le dossier `static` de ce numéro (i.e. `static/numero/12/`). Pareil pour l'icône (qui doit avoir comme taille au moins 70px de haut), qui doit s'appeler `icone-pdf.png`.

### Un souci ?

Regardez dans les dossiers des numéros précédents ce qui avait été fait ;)