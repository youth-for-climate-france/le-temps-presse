+++
date = "2019-05-19T19:54:19+02:00"
draft = false
title = "Éditorial"
order = 1
+++

Bonjour à vous, militants et militantes pour le climat !

Tout d’abord, qu’est-ce que Le Temps Presse ? Un mensuel écolo, qui remplit à la fois les fonctions de sensibilisation, de dénonciation, de vulgarisation, d’information et de divertissement autour du climat. Vous y trouverez de nombreuses rubriques : dossiers, actualités et revendications du mouvement, petits gestes écolos du quotidien…

Ce journal a été concocté bénévolement par des jeunes de Youth For Climate France, mais ne représente en aucun cas l’opinion générale et nationale du mouvement. Les équipes de rédaction, de graphisme, de mise en page et d’informatique ont travaillé d’arrache-pied pour vous offrir ce contenu. Nous ne sommes pas des journalistes professionnels, mais seulement des collégiens, lycéens et étudiants voulant sensibiliser un maximum de personnes à la cause climatique, et ayant la volonté de faire bouger les choses.

Des remarques à nous faire ? Des avis à nous donner ? Des contributions à proposer ? Ou simplement le besoin ou l'envie de nous contacter ? Envoyez-nous un e-mail à l’adresse suivante : <letempspresse@protonmail.com>

Sur ce, nous vous laissons profiter de votre journal, et n’oubliez jamais : le temps presse. Bonne lecture !

{{< author "L’équipe journal de Youth for Climate France" >}}