---
title: "Youth For Climate France : origines et objectifs"
date: 2019-05-21T17:25:46+02:00
draft: false
order: 5
---

{{< figure src="yfc.png" width="50%" >}}

## Les origines du mouvement

Avec seulement sa pancarte et sa détermination, c’est Greta Thunberg qui a allumé la première flamme de notre mouvement. 

Devant le parlement suédois, depuis le 20 août 2018, la jeune lycéenne fait la grève tous les jours, pendant les heures de cours, afin de dénoncer l’inaction de son gouvernement face à l’urgence climatique. 

Depuis les élections générales, en septembre, Greta a continué de manifester chaque vendredi, sans relâche. 

Cette lycéenne peu banale, dont l’histoire a capté l'attention des médias, appelle les collégiens, étudiants et lycéens de chaque pays à manifester avec elle le vendredi, afin d’obliger nos politiques à agir, avant qu’il ne soit trop tard. 

C’est ainsi que deux jeunes belges, inspirées par l’appel de Greta Thunberg, invitent les étudiants à faire grève chaque jeudi, dans une vidéo publiée en décembre 2018 sur Facebook. Le mouvement Youth For Climate (YFC) naît ainsi en début janvier 2019, et prend rapidement de l'ampleur.

En France, c’est de fil en aiguille que la branche YFC s’est construite : des jeunes soucieux de l’avenir de notre Terre, se rendant compte que rien n’était réellement organisé pour la « grève mondiale pour le climat » du 15 mars en France, se sont lancés dans les préparatifs. Le facebook YFC France se lance alors, des appels, des Communiqués de Presse, des cartographies sont créées…

Peu à peu, les différentes villes françaises se mobilisent, formant une véritable connexion entre les différents groupes d’action. 

Aujourd’hui, Youth For Climate France est animé par un sentiment d’urgence. Nous savons que nous manquons de temps. Nous savons que nous vivrons l’apocalypse. C’est pourquoi nous continuerons à nous battre, tant que la France ne daignera prendre de vraies mesures pour sauver la vie sur Terre.

## Depuis la création de Youth For Climate

Depuis la création de Youth For Climate, de nombreuses actions ont été menées, que ce soit localement, nationalement, ou même internationalement. Et depuis, le mouvement français ne chôme pas !

Le 22 février dernier, une marche a été organisée pour le climat à Paris, et Greta Thunberg a pu en faire partie, aux côtés des collégiens, lycéens et étudiants parisiens.

Le 15 mars, la grève mondiale pour le climat a rassemblé plus de 1,7 million de personnes dans 123 pays du monde ! Et en France, nous étions plus de 200 000 à nous mobiliser !

Tous les vendredis, les groupes locaux continuent leur mobilisation, en organisant diverses actions, comme des marches, des clean-walks, des protestations symboliques, des interventions dans certaines grandes surfaces...

Le week-end du 13-14 avril, les assises nationales ont eu lieu à Nancy, afin de pouvoir discuter de l’avenir du mouvement, se former et se rencontrer.

Le 10 mai dernier, dans 27 villes de France, des light-off, consistant à éteindre les lumières des vitrines des magasins allumées pendant la nuit, ont été organisés afin de dénoncer le gaspillage énergétique et la pollution lumineuse entraînés par certaines boutiques et grandes enseignes.

Et tout est à venir ! Vous en avez sûrement entendu parler : le 24 mai, la nouvelle grève pour le climat aura lieu partout dans le monde, encore plus grande que celle du 15 mars !

Et de nouvelles actions sont prévues prochainement, comme le rassemblement à Aix-la-Chapelle le 23 juin, et les prochaines assises, internes à YFC, à Bordeaux, le 13 et le 14 juillet.

## Les buts de Youth For Climate

Vous vous demandez sûrement : quels sont les buts de Youth For Climate ? Qu’est-ce que les membres de Youth For Climate veulent exactement ? À cela, nous pourrions peut-être vous répondre que nous voulons changer le monde, mais cela serait trop prétentieux. Des jeunes qui se mobilisent pour le climat, c’est « tout » ce que nous sommes après tout. Oui mais voilà, nous avons des buts et des revendications : certaines sont mondiales et/ou nationales mais d’autre sont aussi locales. 

Notre premier but mondial est le suivant : faire suivre et respecter les Accords de Paris car depuis 4 ans, rien n’a changé ! Nous attendons encore et toujours que chacun des États respecte son engagement !

Nous souhaitons, au niveau national et mondial, obtenir une justice climatique immédiatement, or cela passe par des actes, plus que des mots. 

Notre second but est de sensibiliser à cette catastrophe est en train de se produire : la 6e grande extinction de masse. Tous les jours des appels sont lancés par les scientifiques du monde entier (cf. rapports du GIEC, de l'IPBES, etc) et pourtant, nos dirigeants et les médias continuent d’ignorer le fait que nous vivons une crise climatique et écologique. C’est pour cela que Youth For Climate France considère la sensibilisation comme l'un de ses buts.

Enfin, à une échelle plus petite, chaque groupe local de Youth For Climate est libre d’avoir ses propres buts et revendications pour sa communauté d’agglomération, son département et/ou sa ville !

What do we want ? Climate Justice !

When do we want it ? Now !
