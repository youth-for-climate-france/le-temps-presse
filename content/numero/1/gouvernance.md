---
title: "Gouvernance au sein de YFC France"
date: 2019-05-21T17:28:49+02:00
draft: false
order: 6
---

Dans une recherche d’une horizontalité au sein de Youth For Climate, le groupe de travail sur la gouvernance a pris des décisions importantes dans le fonctionnement de l’organisation. Deux organismes ont été créés : le cœur et l’Assemblée. Leurs rôles vont donc être détaillés dans cet article.

## Le Cœur

{{< figure src="lien-coeur.svg" width="60%" >}}

Son objectif est simple : permettre la fluidité de la transmission des informations entre les groupes de travail, à travers des liens cœurs, qui sont au nombre de 2 par groupe. Ils sont élus après des élections sans candidats et leurs mandats sont définis par les électeurs. Chaque lien cœur est actif au sein de son organe, il est responsable de transmettre l'information, les besoins, les requêtes, indépendamment de ses opinions personnelles.

Les liens cœur participeront à des réunions opérationnelles, qui devront fluidifier la circulation des besoins de chaque « patate » et répondre à la question « qui a autorité pour ... ».

## L’Assemblée

{{< figure src="assemblee.svg" width="60%" >}}

Cette assemblée fait partie du processus de prise de décision et en est l’un de ces acteurs. Les délégués sont élus par groupe local et comme pour les liens cœur, leur mandat est décidé au moment du vote. Chaque groupe doit également décider de la forme de nomination selon sa taille.

L'assemblée se chargera de voter en leur nom ou au nom de leur groupe local, selon les décisions.

{{< figure src="caricature-herissons.png" width="100%" >}}