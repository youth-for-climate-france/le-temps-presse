---
title: "Soutien aux forgerons et aux cyclistes amateurs"
date: 2019-05-19T21:52:45+02:00
draft: false
order: 2
---

_Appel à la grève mondiale pour le climat du 24 mai_

Les océans sont chauds comme la braise, les forêts brûlent comme des cathédrales. Les climatologues et autres oracles sont sous antidépresseurs, les poissons et les coraux meurent  étouffés dans des sacs plastiques ou à l’acide d’une eau trop chargée en dioxyde et métaux lourds. Les orangs-outans ont perdu la bataille contre la pâte à tartiner goût noisette, les abeilles ont perdu la bataille contre le non-saint Bayer-Monsanto. Les oiseaux, fatigués de migrer, abandonnent leur voyage pour dormir à jamais dans une nappe de pétrole ou entre les quatre roues du dernier SUV à la mode. Les p’tits poulets et les p’tits cochons, après une enfance passée sous une cloche en métal et une adolescence dans les excréments de leurs congénères, finissent dans un bucket XXL ou dans la gamelle de leur descendance. Mais bon, tout ça tu le sais sûrement déjà. Pas la peine d’en faire tout un plat.

Ce que tu ignores sans doute, c’est que ce tas de bois qui se consume et qui s’appelle la planète Terre, ne brûlera pas pour toujours. La Terre, c’est une grande, elle s’en sortira bien toute seule. Et si on ne lui laisse que ça comme solution, elle va s’allumer un bon barbecue dans lequel elle fera mijoter tous ceux d’entre nous qui n’auront pas les moyens de se construire un bunker ou d’échapper aux cyclones et aux sécheresses, et pour assaisonner elle y ajoutera la majeure partie des espèces vivantes. Et quand elle sera repue, elle  éteindra les braises et repartira comme si de rien n’était.

{{< figure src="velo.svg" width="50%" >}}

Un barbecue, habituellement, ça ne se refuse pas... Mais cette fois c’est chacun d’entre nous qui pourrait finir en brochettes. Alors si on te disait que le mur en briques dans lequel on fonce a été construit par une poignée de cuiseurs de merguez inconscients rendus fous par l’argent et le pouvoir, n’aurais-tu pas envie de quitter cette fête un peu glauque ? Si on te disait qu’à plusieurs et avec beaucoup de détermination on peut mettre un grand coup de guidon pour éviter le mur de briques et se prendre le tas de bottes de foin qui est juste à côté, ne préfèrerais-tu pas foncer dans la paille ? Et si on ajoutait que beaucoup d’entre nous, peu férus de merguez, sont déjà partis en vélo casser des briques et sonner les cloches de nos responsables un peu endormis à coups de marteau, n’irais-tu pas toi aussi faire un tour à la forge pour t’en faire un à ta taille ?

Si toi aussi tu es plus tenté par le menu vélo et coups de marteau que par le menu barbecue et écocide, si l’envie te prend de rejoindre un combat qui ferait pâlir les dragons et les marcheurs blancs, un combat qui pourrait sauver des millions de vies d’une mort injuste et permettre à toi et à tes enfants de voir des forêts et des grands singes ailleurs que dans des livres, rejoins l’armée verte le 24 Mai dans la rue. Et si tu es sceptique, incrédule, résigné ou juste complètement déprimé, reste un peu avant de rentrer chez toi pour discuter avec les plus aguerris et en apprendre plus sur ce qu’on peut construire ensemble !

À bientôt derrière le mur de paille.