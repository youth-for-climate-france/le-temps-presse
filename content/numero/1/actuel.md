---
title: "Situation actuelle"
date: 2019-05-20T00:33:55+02:00
draft: false
order: 3
---

L'humanité fait la forte tête. Face à ce que l'on peut appeler aujourd'hui la sixième extinction de masse, avec une disparition des espèces 1000 fois supérieure à la normale, citer des chiffres ne suffit pas pour faire réagir. Les changements vitaux pour notre survie sont sans cesse reportés au lendemain. Comme si l'on ne manquait pas déjà de temps. Mais maintenant, le changement est pressant. Il est vital depuis bien trop longtemps.

Chaque seconde, la fonte des glaces en Antarctique entraîne une perte d'environ huit tonnes de sa masse, soit 8,7 millions de litres d'eau déversés par seconde dans les océans. La fonte totale des glaces de l'Antarctique entraînerait une élévation du niveau des océans d'environ 57 mètres. Je vous laisse essayer d'imaginer la catastrophe sur les îles et les littoraux.

Chaque minute, près de neuf millions de tonnes de déchets plastiques pénètrent dans les océans. Quelle ironie du sort, quand on sait que plus de 50 % de notre oxygène y sont directement liés. Chaque heure, environ trois espèces disparaissent définitivement. De nos jours, plus de la moitié des animaux a disparu, avec plus d'un tiers des oiseaux sur le territoire français, favorisant la prolifération des insectes invasifs qui détruisent nos récoltes.

Et ça ne s'arrête pas là.

À vous qui nous lisez aujourd'hui, pensez-vous réellement que vivre dans cette société consommatrice, consommable, factice et non-durable vaille le coup, si cela implique d'annihiler toute chance de survie à l'humanité, à vous-même comme à vos enfants ?

Changeons aujourd'hui, afin d'apercevoir un nouvel horizon, moins sombre.

Demain, il sera trop tard.

Ce n'est pas la planète qu'il faut sauver, mais nous-mêmes.

## Augmentation des températures

Tic, tac, tic, tac… c’est cuit ! 

Quand on parle climat, la montée des températures est sur toutes les lèvres. 

Mais qui dirait non à un été plus long et plus chaud, pour se la couler douce un peu plus longtemps à la plage ? Sauf que lorsque nous nous attardons sur le sujet, cela laisse penser qu’il faudrait peut-être aller ranger ses sandales et s’activer, pour sortir le monde de la fournaise dans laquelle nous l’avons plongé. Car il pourrait bien être en train de cramer.

{{< figure src="caricature-arbre.png" width="80%" >}}

Si dans 10 ans, nous n'avons pas réduit de 45% les émissions de CO2 au niveau mondial, et de 100 % d’ici 2050, le scénario le plus positif, celui des +1,5°C par rapport à l'ère pré-industrielle, sera largement dépassé. Avec ce scénario, qui reste le meilleur d’entre tous, les zones les plus prospères des États Unis se transformeraient en désert, même si l’adaptation serait toujours possible pour la plupart d’entre nous. Sachant que nous sommes déjà actuellement à + 1,25 °C environ, cela ne nous laisse que très peu de marge de manœuvre.

Mais nous sommes dans la réalité, et même si tous les pays ayant signé l’accord de la COP21 respectaient leurs engagements, il faudrait s’attendre à être confronté à une élévation de + 3 à + 4 °C en 2100. Je vous vois venir, mais sachez que le changement climatique n’attendra pas jusqu’au 31 décembre 2099 pour se manifester. Il est déjà là. Vous rappelez-vous de l’été caniculaire de 2003, responsable de 15 000n morts en France ? Cela deviendrait un phénomène… habituel. De nombreuses grandes villes, comme New York, seraient rayées de la carte, au même titre que quasiment toutes les îles du Pacifique. Un climat chaud qui n’a pas existé depuis près de trois millions d’années, voilà dans quoi nous sommes en train de nous enfoncer, dès maintenant. Les plus grands fleuves du monde s'assècheraient, tandis que la forêt Amazonienne, symbole de la vie et poumon de notre planète, ne serait plus qu’une vaste étendue désolée et aride.

Et attention, le scénario ci-dessus est encore bien positif. Compte tenu de notre trajectoire actuelle, nous fonçons tout droit vers une augmentation de la température de 5 °C ! C'est l'amplitude qui nous sépare de l'âge glaciaire, mais cette fois-ci en sens inverse. Avec 5 degrés de plus, la machine terrestre se brisera. L’humanité pourrait être totalement anéantie.

Un monde apocalyptique se dessine. Et cette fois-ci, ce n’est pas une fiction.

Vous le savez désormais, l’enjeu n’est pas uniquement de sauver les fleurs et les abeilles. Mais nous avons  aujourd’hui toutes les clés en main pour limiter la casse. Nous sommes de plus en plus à nous rendre compte de l’urgence climatique, à nous rebeller. Car chaque millième de degré en moins sur le thermomètre planétaire représente un espoir énorme, pour limiter la hauteur de notre chute. 

Dès maintenant, cessons alors d’entretenir le pouvoir capitaliste qui nous vend un soi-disant « progrès » , tout en déchirant notre avenir.

Dès maintenant, c’est à nous de faire monter la pression, de provoquer le changement.

À nous de choisir entre la survie de l’humanité et son extinction.

À nous de lancer l’alerte !

### Sources :
- _Les chiffres alarmant de WWF sur la pollution des océans par les plastiques_, Atlantico
- _Disparition d'espèces dans le monde_, Planétoscope
- _Fonte des glaces en Antarctique_, Planétoscope
- _Climat : l’illusion démobilisatrice du scénario à 1,5°C_, Libération
- _Climat : les trois scénarios du réchauffement de la planète_, Le Journal du Dimanche
- _Avec 3°C de plus, l'emballement climatique irréversible_, Médiapart
- _Climat : vers 4 à 5 degrés de plus à la fin du siècle à Paris_, Le Monde
- _Comment tout va s’effondrer_, Julien Wosnitza