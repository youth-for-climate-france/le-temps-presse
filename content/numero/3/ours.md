---
title: "Ours"
date: 2019-08-01T19:14:31+02:00
draft: false
order: 11
---

Le Temps Presse - n° 3 - juillet 2019

Directrice de publication : Adèle Boitard-Crépeau

Réalisation : Axel, Marie, Louen, Camille, Alice, Azeline, Alexis, Louise

<letempspresse@protonmail.com>	<http://letempspresse.youthforclimate.fr/>

{{< figure src="yfc.png" width="40%" >}}