---
title: "Et dans le monde ?"
date: 2019-08-01T18:32:31+02:00
draft: false
order: 4
dossier-fin: true
---
 
Cette année, l'Europe a battu des records de température. Mais l'Europe, comparée à ses voisins, c'est comme un petit nid tempéré, perché sur le bon versant de notre planète terre. Il suffit de regarder vers l'Inde par exemple, pour s'en rendre compte.


{{% encadre %}}

### Astuces, suite

- Alternatives à la climatisation : bien que des progrès aient été faits, le climatiseur reste un des appareils les plus énergivores (bien devant le réfrigérateur et selon les marques souvent même plus qu’un four). D’ailleurs, ils consomment près de 30 fois plus que les ventilateurs. Ceux-ci souvent jugés peu efficaces car seulement capables de brasser de l’air chaud, peuvent être améliorés grâce à une astuce simple : disposer une bouteille d’eau glacée devant, ou des linges mouillés dessus, ce qui permettra de refroidir efficacement l’air envoyé. Il existe d’ailleurs d’autres ruses efficaces pour votre intérieur : gérer l’organisation des rideaux et volets. L’idée est de les laisser clos/abaissés en journée, pour éviter aux rayons et à l’air chaud de chauffer  l’intérieur (les rideaux, persiennes et autres draps laissent d’ailleurs filtrer la lumière, pour éviter de tout allumer) et de les ouvrir la nuit (ainsi que les fenêtres, mais gare toutefois aux moustiques), afin de faire circuler l’air frais.
- Les végétaux, nos amis de toujours : les plantes d’intérieur, même les cactées, ont le pouvoir agréable d’absorber l’humidité. Qu’elles soient en pot à l’intérieur ou sur les balcon, n’hésitez pas à en mettre partout. - Manger des fruits et des légumes (surtout les crudités) : de préférence froids, ils hydratent bien (voir les conseils contre la déshydratation au-dessus, toujours d’actualité), et rafraîchissent efficacement le corps. -  Débrancher les appareils électriques inutilisés, qui apportent de la chaleur inutilement. - Pour dormir, n’hésitez pas à mettre le matelas au sol (si votre santé vous le permet), où la température est la plus basse. De la même manière, si vous dormez à l’étage, déménagez quelques temps au rez-de-chaussée.

{{% /encadre %}}

Comparons donc nos ventilateurs énergivores à la maigre ombre des arbres du Rajasthan, où en juin dernier le mercure a dépassé les 50°C. Ces 10 dernières années les vagues de chaleur se sont multipliées en Inde entraînant la mort de 5 000 personnes. Car c'est un fait, la chaleur tue. Et aujourd'hui, elle s'amplifie. Notre planète est prise par le virus moderne du réchauffement climatique. Sa fièvre est montée de 1°C de plus depuis l'ère préindustrielle et aucun traitement sérieux ne ui a été prescrit, si ce ne sont les accords de Paris en 2015. 

Pourtant, il y a urgence. 30% de la population mondiale est exposée à des vagues de chaleur potentiellement meurtrières 20 jours par an, voire plus.

{{< figure src="inde.jpg" width="50%" >}}

Cette année, le nord de l'Inde a suffoqué pendant 34 jours sous 40°C,  l'une des plus longues canicules de son histoire. Le pays est entré dans une véritable crise de l'eau, dans la région du Bihar, au moins 80 morts ont été recensés. Et si nous jetons notre regard vers le nord, la situation n'y est pas moins funeste. Des records de température ont été relevés en Alaska en juillet, avec plus de 30°C sur la banquise, c'est à dire la même température que dans les saloons du Texas. Le permafrost, couche du sol en théorie gelée toute l'année, est en train de fondre inexorablement. Un bouleversement majeur pour les espèces animales  et les communautés qui y vivent. Cependant toutes ces observations sont encore bien douillettes face aux prévisions des scientifiques pou 2050 et 2100. Le réchauffement climatique promet d'être bien plus meurtrier que la peste médiévale qui a emmenée dans son sillage un tiers de la population mondiale.
Le monde est malade, et c'est à nous tous de le guérir.