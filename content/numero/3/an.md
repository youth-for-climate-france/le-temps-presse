---
title: "Youth for Climate à l'Assemblée Nationale"
date: 2019-08-01T18:47:42+02:00
draft: false
dossier-debut: "actu"
dossier-titre: "Questions d'actualité"
order: 6
---

Le 23 Juillet 2019, Ivy Fleur-Boileau, Alicia Arquetoux, Virgile Mouquet, trois membres de Youth For Climate Angers, Lorient, et Bordeaux, ainsi que Greta Thunberg, lanceuse d’alerte Suédoise, et la vice-présidente du GIEC Valérie Masson-Delmotte, ont tenus des discours en face d’un certain nombre de députés, dans la Salle Victor Hugo de l’Assemblée Nationale.

{{< figure src="greta.jpg" caption="Crédit photos : Louise V." width="50%" >}}

Ils étaient invités par le député Matthieu Orphelin à s’exprimer et à prendre part aux débats avec les élus. Malgré le boycott de l’intervention de Greta Thunberg, et donc des autres jeunes et de scientifique, les questions et remerciements post-discours furent nombreux.

{{< figure src="ivy.jpg" width="60%" caption="Crédit photos : Louise V." >}}

Après le discours de Greta Thunberg, où elle cite énormément de chiffres et de faits politiques, tout en évoquant l’urgence, le tour est venu pour les 3 jeunes français de s’exprimer. Ils ont par exemple demandé aux députés de prendre leurs responsabilités « car les citoyens vous ont élus. Prendre des décisions contraignantes est votre devoir ! Nous vous demandons de faire vos devoirs » disait Virgile. Ivy a parlé de l’angoisse douloureuse qu’elle et beaucoup d’autres ressentent. « Moi, ça me fais paniquer 2030 2040 (...) Combien d’enfants dans la rue vous faut-il pour vous faire réagir ? » a-t-elle déclaré. Enfin, Alicia Arquetoux a rappelé ladignité du combat porté par les jeunes en lançant « qu'y a t’il de plus légitime que de se battre pour sa vie ? » notamment. 

{{< figure src="an1.jpg" width="70%" caption="Crédit photos : Louise V." >}}

Valérie Masson-Delmotte, a pris la parole juste après les jeunes pour développer les aspects scientifiques la question. Elle a notamment expliqué que "Limiter le réchauffement à 1,5 degré pourrait réduire de plusieurs centaines de millions le nombre de personnes dans le monde exposées au risque climatique et susceptibles de basculer dans la pauvreté", et le fait qu'il est encore possible d'atteindre cet objectif. Elle a rappelé que « chaque demi degré compte ».

{{< figure src="an2.jpg" width="70%" caption="Crédit photos : Louise V." >}}

A la suite des discours, est venu le temps des questions. Les jeunes de Youth for Climate et Greta Thunberg répondent ainsi aux députés et aux médias. Au milieu des interventions, Robin Jullian, membre de Youth for Climate, qui fait la grève tous les vendredis à Grenoble, a partagé son touchant témoignage en expliquant le choc qu'avait provoqué dans son enfance sa réalisation que son futur était menacé. Il déclare avoir « perdu foi en l'Humanité », mais que cette douleur lui a servi de moteur : « J'ai compris qu’abandonner ferait de moi un lâche ».
