---
title: "Le plan national Santé-Environnement-3, quelles sont les causes de son inefficacité ?"
date: 2019-08-01T19:10:12+02:00
draft: false
order: 10
dossier-fin: true
---

{{% chapo %}}
Vers avril 2019, deux rapports sur le PNSE-3 ont été publiés. Un par l’IGAS (Inspection générale des affaires sociales) et un autre par le CGEDD (Conseil général de l’environnement et du développement durable). Tous deux montrent l’inutilité de ce plan national. 
{{% /chapo %}}

## Quel intérêt et quel bilan pour le PNSE-3 ? 

Pour savoir ce qu’est un plan santé-environnement, il faut d’abord définir la santé-environnement. Selon l’Organisation Mondiale de la Santé (OMS) ce sont “Les aspects de la santé humaine et des maladies qui sont déterminés par l’environnement. Cela se réfère également à la théorie et à la pratique de contrôle et d’évaluation dans l’environnement des facteurs qui peuvent potentiellement affecter la santé.”  

En France, il y a un PNSE tous les 5 ans depuis 2008, il a pour rôle de réduire la pollution et l’exposition de la population à celle-ci. Il est coordonné par les Ministères de l’Environnement et de la Santé. 

Si ce dernier plan santé-environnement a doublé le nombre de mesures prises (110), le résultat est très décevant. Le rapport fait par l’IGAS considère qu’il n’y a aucun impact positif mesurable. De plus le nombre de mesures prises relève essentiellement de la communication et non d'une réelle volonté d’action. Il est estimé que seules 20 % des mesures répondent aux risques de santé-environnement.

{{< figure src="pnse3.jpg" width="70%" >}}

## Pourquoi une telle inefficacité ? 

On peut identifier plusieurs facteurs, relevant majoritairement d’un faible investissement. En effet, le rapport de l’IGAS pointe le manque de moyens humains, financiers et organisationnels. Il révèle aussi que le PNSE-3 n’a pas de budget propre et qu’aucun chiffrement des coûts n’a été fait.  

Le plan national comprenait aussi des Plan Régionaux Santé-Environnement (PRSE) qui avaient les mêmes objectifs que le PNSE et encore une fois l’IGAS rapporte que “les PRSE sont structurellement peu intégrés dans les politiques publiques conduites par l’Etat comme par les collectivités territoriales” et qu’ils “ne mobilisent que des montants modiques”. Ainsi, ils ne semblent faire preuve de plus d’efficacité que leur homologue national. 

Le prochain PNSE est en cours d’élaboration, nous espérons donc qu’il saura faire face aux problèmes de santé-environnement qui tendent à se multiplier, de meilleure manière que son prédécesseur. 
