---
title: "Les effets de la canicule sur la santé"
date: 2019-08-01T18:04:25+02:00
draft: false
order: 3
---

{{% chapo %}}
Le corps humain (avec une moyenne corporelle de 37 degrés Celsius), n'est pas adapté à des températures aussi élevées. Ainsi, la chaleur peut avoir des effets très néfastes, dont deux majeurs : la déshydratation et les "coups de chaleur".
{{% /chapo %}}

## La déshydratation

{{% encadre %}}

### Astuces

- Le plus important, boire, au moins 2 litres d’eau par jour. L'astuce : toujours avoir sa gourde sur soi, facile à remplir et écologique. Attention, l'eau glacée peut causer un choc thermique et le corps réagit souvent en créant de la chaleur pour supporter la différence. Une eau fraîche est suffisante et même plus efficace. Les fruits sont aussi un bon complément pour l'hydratation, même s’ils ne remplacent pas l'eau.
- Le brumisateur, pour bien s'hydrater. Pensez à prendre une bouteille réutilisable, à remplir à la maison chaque matin !
- Si vous avez une piscine (petite, grande, gonflable ou carrelée), une mer ou un océan à portée de main ou de pieds, n'hésitez surtout pas. Rien de mieux qu'une bonne baignade pour se rafraîchir (en faisant attention à bien se mouiller la nuque, gare à l'hydrocution). Dans le cas contraire, un bon linge humide (jamais glacé, les pores et les vaisseaux sanguins se resserrent et la fraîcheur circule moins) sera suffisant pour un rafraîchissement optimal (et moins consommateur qu'une douche).

{{% /encadre %}}

La déshydratation est assez explicite : c'est le simple fait d'avoir perdu une quantité excessive de l'eau contenue dans notre corps. Elle se caractérise par une soif intense et une sécheresse de la peau. Ses causes principales sont les virus diarrhéiques, et, dans le cas qui nous intéresse, la chaleur. En fait, celle-ci crée une réaction d'adaptation du corps, qui tente de s'acclimater aux températures plus élevées qu’habituellement. Il y a donc une recrudescence de la transpiration, on respire plus rapidement et les vaisseaux sanguins augmentent de diamètre pour refroidir le sang. Le problème, c'est que le phénomène de transpiration (rempart numéro un) exige une hydratation régulière et constante (en moyenne 2 litres par jour pendant les canicules), et si possible que l'air soit en mouvement autour afin de faciliter la disparition de la sueur. Or, pour plusieurs catégories de la population, c'est souvent mission impossible. Par exemple, les personnes de plus de 65 ans sont souvent moins sensibles à la chaleur et ressentent moins l'envie de s'hydrater, que des adultes en bonne santé. Elles encourent donc plus de risques, tout comme les enfants, les bébés ainsi que les femmes enceintes qui ont besoin d’une hydratation supérieure à la moyenne. Par ailleurs, les gens sous le seuil de pauvreté ou en difficulté financière n'ont pas forcément les moyens d'acheter ventilateurs, climatiseurs et autres ; eux aussi risquent donc beaucoup en cas de canicule.

## Le coup de chaleur

Le deuxième danger lié à la canicule est le coup de chaleur, ou hyperthermie. L'hyperthermie est le contraire de l'hypothermie, c'est-à-dire que dans ce cas la température du corps augmente. Pour l'être humain, le risque mortel intervient quand le corps atteint ou dépasse les 41,5 degrés Celsius. Le coup de chaleur existe en trois "versions" différentes, chacune caractérisée par la source de l'hyperthermie : en premier, l'insolation résulte d'une trop longue exposition au soleil ; en second, le coup de chaleur d'exercice découle d'un effort trop intense, allié à de mauvaises conditions d'hydratation et/ou d'évacuation de la chaleur ; et pour finir, le coup de chaleur classique peut intervenir lors d'une importante exposition à la  chaleur ambiante (canicule, incendie ...). Cependant, lors d'une canicule, le risque de succomber à un coup de chaleur, quel qu'il soit, est bien plus élevé, puisque ces épisodes arrivent souvent en été : ensoleillement accru, compétitions sportives fréquentes à cette saison... Il faut donc se protéger et prêter attention aux différents symptômes (nausée, vomissement, maux de têtes et fatigue accrue).

Comme pour la plupart des pathologies, les nourrissons, les enfants et les personnes âgées y sont extrêmement sensibles. Il est donc important de prendre soin d'eux et de régulièrement prendre de leurs nouvelles.