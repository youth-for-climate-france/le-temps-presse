---
title: "La canicule va-t-elle devenir une habitude ?"
date: 2019-08-01T17:46:48+02:00
draft: false
order: 2
dossier-debut: "canicule"
dossier-class: "orange"
dossier-titre: "Dossier : Les canicules ne nous emballent pas..."
---

Au mois de juin dernier, la canicule a fait parler d’elle, c’est le moins qu’on puisse dire. Au point de rappeler à l’esprit de certains ce « faux » bulletin météo, publié il y a quelques années par Météo France. Cette vidéo nous montrait à quoi s’attendre en été 2050, quand les conséquences du réchauffement climatique se feront sentir, bien plus que maintenant. Mais doit-on véritablement s’attendre à ce que ce que l’on nomme aujourd’hui une canicule devienne une habitude ?

Tout dépend. Tout dépend de ce que l’on fait pour freiner le réchauffement climatique. Tout dépend de combien augmente la température moyenne à la surface du globe. Depuis le début du 20ème siècle, il y a déjà 1,1 °C de différence. Certains pensent que ce n’est pas grand-chose. En réalité, c’est déjà un problème. Certaines espèces migrent de 20 cm par heure (en moyenne) pour échapper à la chaleur, d’autres sont en train de voir leur nombre chuter. Mais ce n’est rien, par rapport à la catastrophe qui s’annonce si nous continuons dans la voie aujourd’hui empruntée par nos dirigeants : ne rien faire, ou si peu.

Soyons clairs : si nous arrêtons, d’ici 2050, de produire plus de gaz à effet de serre  (GES) que ce que les arbres et l’océan ne peuvent en absorber, la quantité de GES déjà présents dans l’atmosphère est telle que nous atteindront quand même 1,5 °C de plus qu’aujourd’hui. Avec ce chiffre, les canicules seraient 2 fois plus fréquentes en France. Bien sûr, c’est sans parler des espèces qui disparaissent encore plus vite et autres « dommages collatéraux ».

<div class="encadre">
<h2>Définition</h2>

Les épisodes de canicule sont des périodes pendant lesquelles une forte chaleur reste constante nuit et jour pendant au moins 72 heures (en France, les seuils sont personnalisés selon les régions). Elle peut s'accompagner d'un taux d'humidité accru (qui augmente la température ressentie) et engendre souvent une augmentation de la pollution de l'air, des risques d'incendie, mais surtout des problèmes de santé.

</div>

Mais ce scénario catastrophe est… optimiste. En réalité, les quantités de gaz à effet de serre que les humains émettent ne cessent d’augmenter. Si la neutralité carbone était atteinte en 2075 (un autre scénario optimiste) alors le réchauffement global serait d’environ 2°C, soit quatre fois plus de canicules en France. Le niveau de l’océan aura augmenté d’un demi mètre. 

Mais, comme le souligne Aurélien Barreau, aujourd’hui, pour avoir un scénario réaliste, il faut plus miser sur une augmentation de 3 à 4 °C. Sauf que là, c’est l’hécatombe : vous pouvez oublier le corail, la banquise, et l’eau potable à volonté. En revanche, dites bonjour aux inondations, aux déserts au sud de l’Europe, au manque de nourriture, et à la canicule qui en France sera présente chaque été… Enfin, malheureusement, il faut s’attendre à ce que l’espèce suivante sur la liste des êtres vivants touchés par le réchauffement au point d’en mourir soit… nous.

{{< figure src="desert.jpg" width="90%" >}}

Alors oui, en l’état actuel des choses, attendez vous à ce que la canicule vienne toquer à votre porte de plus en plus souvent. De plus, chers lecteurs, il est probable qu’elle vous touche tout particulièrement : le réchauffement climatique est une bombe à retardement qui empire avec le temps. Par exemple, personnellement j’ai 16 ans. Si je suis encore en vie d’ici 2075, j’aurais 72 ans. Non seulement je peux aujourd’hui me dire que je n’atteindrais probablement pas cet âge, pour diverses raisons liées aux activités humaines, mais je peux en plus me dire que d’ici là, la canicule sera plus chaude et plus fréquente. A 72 ans, je serai plus faible. A 72 ans, la canicule pourrait tout bonnement me tuer, et en tuer d’autres.

A ce rythme-là, ça risque d’être de plus en plus difficile d’être plus chaud que le climat.