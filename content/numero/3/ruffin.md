---
title: "François Ruffin veut supprimer certains vols intérieurs"
date: 2019-08-01T18:56:06+02:00
draft: false
order: 7
---

François Ruffin, député la France Insoumise (FI) de la Somme, a déposé le 5 juin une « proposition de loi visant à remplacer les vols intérieurs par le train (quand c’est possible…) », avec, entre parenthèses, une petite pique à l’encontre du gouvernement et de sa politique ferroviaire. Cette proposition, soutenue par une quinzaine de députés, principalement de la FI, mais aussi un élu socialiste, pourrait être examinée d’ici la fin de l’année 2019.

{{% encadre %}}

## En parallèle...

Depuis le dépôt du texte, le gouvernement a lui aussi décidé de faire un pas pour lutter contre la pollution du transport aérien. Un pas qui va bien moins loin que ce qui est proposé par le groupe FI. À la suite du deuxième conseil de défense écologique, le 9 juillet, Elizabeth Borne, ministre des transports, a annoncé la mise en place d’une « écocontribution », pour ne pas employer le mot taxe, sur les vols au départ des aéroports français. Selon la distance parcourue et la classe utilisée, cette taxe coûtera entre 1,5 et 18 €. Cette mesure, qui devrait être appliquée en 2020, pourrait rapporter 180 millions d’euros par an à l’État, qui seront reversés au financement des transports du quotidien. Cette annonce suscite déjà de vives réactions de compagnies aériennes, notamment Air France qui dénonce une taxe qui « pénaliserait fortement sa compétitivité ».

Le ministère de la transition écologique avait d’abord réfléchi à la mise en place d’une taxe sur le kérosène pour pallier le coût des mesures annoncées dans la loi Énergie-Climat (dont nous vous parlions dans le précédent numéro). La mesure a vite été abandonnée, elle aurait défavorisé les transporteurs français face aux compagnies étrangères. Mme Borne a donc préféré annoncer une mesure allégée, qui fera pas ne vraiment diminuer le trafic aérien.
{{% /encadre %}}


En France, on observe depuis quelques années le développement important des vols intérieurs, les vols entre deux villes d’un même pays, que les utilisateurs empruntent, au détriment du train, diminuant ainsi leur temps de voyage, mais aussi souvent le coût du trajet, mais en augmentant leur empreinte carbone.


François Ruffin constate plusieurs injustices autour de l’avion : une injustice sociale − « le transport aérien bénéficie surtout aux plus aisés » − ; une injustice territoriale − l’offre des transports ferroviaires du quotidien s’effrite, l’avion ne profite qu’aux grandes villes − ; et une injustice fiscale − en partie palliée par les dernières annonces du gouvernement. Mais il y aussi bien sûr un problème écologique. Le transport aérien est responsable de 2 à 3 % des émissions de gaz à effet de serre (GES) mondiales. En moyenne, selon l’Agence de l'environnement et de la maîtrise de l’énergie (ADEME), l’avion en émet 40 fois plus que le train, et 2,5 fois plus que la voiture. « Un aller-retour Paris-Marseille [une des lignes les plus fréquentées de France, avec plus de 1,6 millions de passagers annuels en avion émet 195 kg de CO2 par passager. Ce même aller-retour effectué en TGV en émet 4,14, soit près de 50 fois moins », détaille le député.

Le député propose donc que « l’autorité administrative interdise ou limite » les vols intérieurs quand une alternative en train « sans correspondance existe, qu’elle permet un temps de trajet équivalent au temps de trajet en avion plus 2h 30 ». Une proposition qui pourrait supprimer, selon les Décodeurs du Monde, 20 lignes régulières sur les 148 lignes de vols intérieurs que compte la France métropolitaine, et 20 sur 102 en France (Corse exclue). Le nombre de passagers serait réduit d’environ 30 %. François Ruffin, estime que la mesure qu’il propose « permettrait d’éviter au moins 1,1 à 1,2 million de tonnes équivalent CO2 par an, soit l’équivalent de l’empreinte carbone de 100 000 personnes ».


{{< figure src="ruffin.jpg" width="50%" >}}

La députée et présidente du parti Génération Écologique Delphine Batho, signataire de la proposition de loi, propose, elle, d’aller plus loin, en interdisant les vols où l’alternative en train dure moins de 5 heures de plus que l’avion, ce qui permettrait la suppression de 41 lignes sur 102.