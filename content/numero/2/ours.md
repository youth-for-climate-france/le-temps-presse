---
title: "Ours"
date: 2019-06-21T19:15:58+02:00
draft: false
order: 11
---

Le Temps Presse - n°2 - juin 2019

Directrice de publication : Adèle Boitard-Crépeau

Réalisation : Hadrien, Alexis, Camille, Clément, Élise, Louen, Alice, Anouck

<letempspresse@protonmail.com>

{{< figure src="yfclogo.jpg" >}}