---
title: "Aachen : grève et action directe"
date: 2019-07-07T23:51:33+02:00
draft: false
order: 10
---

### \#AC2106

Pourquoi ce hashtag, me diriez-vous ? C’est tout simple, il représente les 2 manifestations ayant eu lieu à Aix-la-Chapelle et le long de la mine de Garzweiler II.

{{< figure src="manifaachen.jpg" width="80%" >}}

Vendredi, près de 40 000 personnes de 16 nationalités ont relié différents points (la Gare Centrale, un parc, et l’université de la ville, mais aussi à vélo des Pays-Bas voisins) au stade Tivoli au nord d’Aix-la-Chapelle. Durant la manifestation ont résonné les slogans comme « on est là, on est bruyant, car ils nous volent l’avenir » ou encore « Charbon stop ; protection du climat ; l’Hambi [forêt menacée] reste ». Au bout de la manifestation, alternaient groupes de musique et intervenants venant de partout dans le monde (Allemands mais aussi indiens et philippins). Du côté français, Cyril Dion, réalisateur du film Demain et Robin Jullian, de Grenoble se sont exprimés. Le soir, un concert est donné par le groupe BrassRiot près du lieu d’hébergement, un parking renommé pour l’occasion en « Parkhotel ». Il a accueilli au total près de 2 750 personnes ! 

Le samedi, une seconde manifestation a eu lieu le long de la mine jusqu’au village de Kayenberg, situé à moins d’un kilomètre de la mine de lignite. Elle aura rassemblé 8 000 personnes ! Dans le même temps les militants d’Ende Gelände (environ 6 000) entraient pacifiquement sur la mine de Garzweiler II pour protester contre les agrandissements de mine RWE prévus, qui détruiraient plusieurs villages et la forêt de Hambach située proche de la mine du même nom à quelques kilomètres du lieu de la manifestation. Ces manifestations auront fait d’Aix-la-Chapelle la capitale du mouvement écologiste pour quelques jours !

## Témoignage

Je m’appelle Zoé, j’ai 18 ans, je suis à  Youth For Climate Lyon et ce weekend j’ai fait plusieurs actions de lutte climatique en Allemagne. 

Le 21 juin je suis allée à Aix la chapelle pour la marche européenne organisée par le mouvement Fridays For Future, qui a rassemblé 16 autres pays et 40 000 personnes ! Ce fut vraiment  impressionnant de voir autant de jeunes engagés pour le climat et une si grande détermination et énergie. 

Mais ce qui a été le plus marquant pour moi fut l’action organisée par Ende Gelände contre l'exploitation du charbon, qui en plus d’être l’une des plus grandes sources de C02 en Europe menace la forêt et les villages environnant les différentes mines.

{{< figure src="mine.jpg" >}}

Samedi j’ai donc réussi à rentrer dans la mine de Garzweiler avec un groupe de 2000 personnes. Ce fut une action en même temps poignante et effrayante. Dans un premier temps lorsque je me suis retrouvée face à cette immensité sans vie, comme un paysage lunaire où tout a été détruit, je me suis réellement rendue compte du désastre. Puis il a fallu passer plusieurs barrages de policiers, courir pour descendre dans la mine et tenir bon malgré la chaleur épuisante et la pression des forces de police commençant à nous déloger. 

Grâce aux différents groupes, la mine et les rails d’acheminement sont restés bloqués pendant 48h. 

Malgré l’euphorie de l’action de masse, tous les moments de partage avec les autres activistes et l’impression d’avoir œuvré pour la planète,  je n’oublie pas qu’il ne s’agit que d’une action ponctuelle et que nous avons encore beaucoup de travail dans la lutte climatique.

Il faut maintenant aller plus loin que de simples manifestations et arrêter de seulement parler des « générations futures » en repoussant toujours le problème car l’urgence est bien réelle et elle nous concerne déjà toutes et tous. La désobéissance civile doit se faire de plus en plus en repensant la balance entre le légal et le légitime : ici bloquer la mine était illégal mais totalement légitime si l’on considère qu’il s’agit d’une menace environnementale. Nous devons conti­nuer à nous unir face à l’urgence climatique, le mou­vement ne fait que com­mencer !

_Merci à *@\_stephanemayer\_* pour les photos !_