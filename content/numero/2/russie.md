---
title: "Arshak Makichyan, le quotidien d'un militant russe"
date: 2019-07-07T23:48:23+02:00
draft: false
order: 9
---

Depuis le 15 mars, un jeune russe, Arshak Makichyan, manifeste chaque vendredi à Moscou contre l'inaction climatique. Il a 24 ans, et étudie le violon au conservatoire de musique. Nous avons eu l'honneur de pouvoir parler avec lui. Arshak  nous raconte l'origine de sa mobilisation et son quotidien : « c'est au moment où Greta fait son appel à manifester que j'ai décidé de m’engager pour le climat. Avant, cela m’avait intéressé, mais j’avais trop peur et j’étais occupé à jouer du violon. » Il a donc décidé de manifester chez lui, à Moscou. Mais, en Russie les rassemblements ne sont pas du tout vus de la même manière : ceux de plus de deux personnes sont interdits.

C’est pourquoi Arshak exprime son mécon-tentement par les piquets solitaires : il se tient debout, devant le monument de Pouchkine (au centre de la ville), sa pancarte exposée aux regards des passants ; certains, intrigués, vont parler avec lui, mais pas toujours pour le féliciter. Plusieurs l’accusent même de véhiculer de la propagande américaine. En effet, en Russie, beaucoup de gens pensent que le réchauffement climatique est une fake news (comme quoi, pas besoin de s’appeler Trump pour croire en cette folie), une idée inventée par les États-Unis : le régime en place passe sous silence cet enjeu, ce qui fait que peu de gens y sont sensibilisés.

« La mobilisation n’est pas très forte dans mon pays, la plupart des gens de mon âge ne sont pas sensibilisés du tout, ils ne savent même pas trier par exemple. » nous explique Arshak ; et les seuls intéressés ont peur des représailles du gouvernement. « Mais nous sommes déjà un petit groupe, qui s’organise et se soutient. »

Un autre problème en Russie est la police : Arshak nous raconte que la police a essayé de l’intimider, par deux fois déjà. La première était lors d’une de ses grèves : « ils sont venus me voir, et m’ont posé toutes sortes de questions insensée : qui me payait pour faire ça, etc. Ils ne répondaient pas à mes questions ; après, ils ont pris une photo de mon passeport et m’ont dit qu’ils allaient en parler avec les autorités. » La deuxième fois était dans le métro : les policiers l’ont emmené dans une salle sans rien dire, ils ont appelé quelqu’un, puis l’ont relâché au bout de quelques minutes.

« Si elle le souhaite, la police peut monter de fausses accusations contre moi, mais pour l’instant, elle a jugé que c’était mieux de me laisser faire, grâce à ma couverture médiatique : ce serait vite relayé à l’international et cela donnerait une trop mauvaise image de la Russie. Malgré cela, je ne suis quand même pas trop rassuré : je suis exposé aux regards malveillants dans la rue, et être seul n’est pas toujours facile. »

Théoriquement les manifestations ne sont pas interdites, en Russie, mais il faut une autorisation pour les faire (et si on fait une manifestation sauvage ce n’est pas comme en France, mais autrement plus dangereux). Le groupe d’étudiants qui organisait le 15 mars avait réussi à avoir l’autorisation : « Moi, je venais d’arriver dans le mouvement, j’ai juste suivi le groupe d’organisateurs. Nous étions à peu près 50, mais c’était une manifestation statique, nous étions tous confinés dans un endroit entouré de barrières. Les passants ne pouvaient pas nous voir. » Mais le 24, la demande a été refusée, alors l’étudiant a manifesté selon sa méthode, en piquet.

Pour l’année prochaine, il a beaucoup de projets : « j’ai commencé à traduire le manifeste de Fridays for Future en russe avec l’aide d’autres organisations et personnes, pour que ce soit ouvert à tous ceux qui ne sont pas à l’aise en anglais. » Arshak compte aussi préparer la grève mondiale, le 20 septembre, et peut-être trouver encore d’autres étudiants russes pour manifester avec lui. Mais, cela ne peut que s’améliorer, après tout : ils sont de plus en plus à manifester !

{{< figure src="russie.png" width="40%" >}}