---
title: "Extinction Rébellion nous sauvera-t-il ?"
date: 2019-07-07T23:25:25+02:00
draft: false
order: 5
---

{{< figure src="XR.jpg" >}}

Vous l’avez sans doute déjà aperçu, ce symbole dessiné de par le monde. Facile à reproduire et à identifier, il a probablement été créé par un artiviste londonien (Goldfrog ESP) en 2011. Avant d’être repris par le mouvement Extinction Rébellion, c’était avant tout le symbole de l’extinction en cours : on y retrouve la planète Terre (le cercle) dans laquelle se trouve un sablier, symbole de l’urgence à agir. Le vert représente le combat écologique, et le noir sa gravité (c’est la couleur du deuil). On peut aussi y voir le X d’Extinction Rébellion (souvent abrégé en XR).

## Un mouvement très récent

C’est en 2018 que le symbole est repris par Extinction Rébellion à sa création. Né au Royaume-Uni, le mouvement n’est, en 2016, qu’une campagne du groupe « RisingUp! ». Cette campagne donnera lieu deux ans plus tard au lancement d’Extinction Rébellion et à la déclaration de rébellion à Londres le 31 octobre 2018 (soit peu après les premières marches pour le climat). Et c’est dès novembre 2018 que le mouvement se fait remarquer, en bloquant les 5 principaux ponts de Londres, étant alors qualifié de « plus grand mouvement de désobéissance civile depuis des décennies » par le journal anglais The Guardian. Suite à cela, le mouvement se diffuse dans plusieurs pays, notamment la France. Ce départ en France est marqué par le Jour de Déclaration de Rébellion qui s’est déroulé le 24 mars 2019. Les actions s’enchaînent ensuite, avec des objectifs très clairs :

- La reconnaissance de la gravité et de l’urgence des crises écologiques actuelles et une communication honnête sur le sujet.
- La réduction immédiate des émissions de gaz à effet de serre pour atteindre la neutralité carbone en 2025, grâce à une réduction de la consommation et une descente énergétique planifiée.
- L’arrêt immédiat de la destruction des écosystèmes océaniques et terrestres, à l’origine d’une extinction massive du monde vivant. (cet objectif a été rajouté par le mouvement français)
- La création d’une assemblée citoyenne chargée de décider des mesures à mettre en place pour atteindre ces objectifs et garante d’une transition juste et équitable.

Répondant à un besoin urgent d’actions efficaces, ce mouvement prône la rébellion contre l’extinction. En d’autres termes, lutter par le biais d’actions de désobéissance civile non violente afin de limiter la sixième extinction de masse et de nous permettre de vivre dans un monde décent. Cette stratégie a été soutenue par de nombreux chercheurs français, suisses, et belges qui ont signés le 20 février 2019 une lettre ouverte dans laquelle est écrit « Nous comprenons un mouvement de désobéissance civile comme Extinction Rébellion, dont la radicalité relève du réflexe de survie »

## Quel consensus d’action ?

Ce mouvement est donc caractérisé par la non violence, mais aussi par une organisation holocratique et décentralisée. En effet, les différents groupes locaux peuvent agir de manière autonome, du moment qu’ils respectent les principes et valeurs d’Extinction Rébellion (voir encadré page suivante). De plus, tout s’organise collectivement puisque les échanges se font via une plateforme ouverte à tous (les ressources auxquelles l’on a accès sont cependant liées au degré d’investissement).

La non violence n’est pas seulement une philosophie, elle n’est pas seulement plus morale que la violence, c’est surtout, selon Extinction Rebellion, plus efficace. En effet, cela force le camp adverse, qui peut difficilement discréditer un mouvement (> non-)violent, à répondre. De plus, cela le met dans un dilemme : soit il laisse passer, soit il réprime. Or, en réprimant (par exemple en envoyant des milliers de personnes en garde à vue), il attire l’attention et fait donc de la publicité pour ceux qu’il veut réprimer. Par exemple, montrer que l’on est prêt à faire des sacrifices pour cesser d’aggraver la menace environnementale peut faire prendre conscience à certaines personnes de son ampleur. Enfin, selon une étude se basant sur 300 mouvement sociaux à partir de 1900, les mouvements violents réussissent moins bien que les autres.

## Zoom sur 3 actions phares

Désobéissance civile (d’après wikipédia) : refus assumé et public de se soumettre à une loi, un  règlement, une organisation ou un pouvoir jugé injuste par ceux qui le contestent, tout en faisant de ce refus une arme de combat pacifique. En effet, comme l’explique La Boétie dans son Discours de la servitude volontaire, le pouvoir d’un État repose entièrement sur la coopération de la population. Ainsi, dès l’instant où la population refuse d’obéir, l’État n’a plus de pouvoir.
Le terme fut créé par l’américain Henri David Thoreau dans son essai La Désobéissance civile, publié en 1849, à la suite de son refus de payer une taxe destinée à financer la guerre contre le Mexique. 

L’action qui a marqué le début d’Extinction Rébellion est donc le blocage des 5 principaux ponts de Londres par environ 6000 personnes. 

Le but était d’attirer l’attention sur l’inaction criminelle de nos gouvernements : chaque jour, 60 espèces disparaissent et si nous ne faisons rien maintenant, il n’y aura pas de lendemain. Ils voulaient inciter chacun à prendre conscience que la responsabilité est partagée, que nous avons tous le pouvoir d’agir. Cette action a donné lieu à 8 arrestations.   

Le 12 avril 2019 à Paris, une action de sensibilisation et de dénonciation de l’industrie textile est menée : les activistes d'Extinction Rébellion ont déversés 1,5t de vêtements usagés devant le magasin H&M rue Lafayette à Paris. En effet, l’industrie textile est la deuxième plus polluante au monde : toxique pour ceux qui y travaillent, pour les écosystèmes, pour la planète… les victimes de la mode sont donc nombreuses. Par exemple, le transport (les vêtements étant souvent faits à l’autre bout du monde) rejette d’énormes quantités de gaz à effet de serre, les industries déversent leurs déchets dans les rivières (et tuent ainsi les écosystèmes et les poissons), les salariés tombent malades à cause des mauvaises conditions… Or H&M est le symbole de cette industrie mortifère, qui tente malgré tout de faire croire que leurs produits sont éco-responsables alors qu’il n’en est rien. Le but était donc principalement d’attirer l’attention.

Du 15 au 21 avril 2019 a eu lieu la semaine internationale de la rébellion : pendant une dizaine de jours, à Londres des milliers de personnes ont bloqué des artères principales, des ponts, des métros (en s’y collant à la glue), des lieux emblématiques de la ville… Une personne qui avait 83 ans ce jour là, est même montée sur le toit d’un métro afin d’alerter sur l’urgence écologique et demander d’arrêter le massacre. Car comme l’a dit un militant d’Extinction Rébellion en réponse à une critique, « des millions de personnes vont mourir de faim ». Au total, environ 1000 policiers ont été déployés, et il y a eu plus de 1000 arrestations. Extinction Rébellion disait cependant rester jusqu’à ce que le gouvernement les écoutent et agisse. Il y a aussi des actions dans d’autres pays, comme en France où le 19 avril, des militants ont bloqué une antenne du ministère de la Transition écologique, les tours Total, EDF et Société Générale. Des actions auraient ainsi eu lieu dans 80 villes de 33 pays.

Mais ces quelques actions ne sont que des exemples. Il y en a eu bien d’autres, pas uniquement dans les capitales, et dans d’autres pays comme le Québec ou les États-Unis. Il reste cependant beaucoup à faire : à Londres, la déclaration de l’État d’urgence climatique (faite peu après la semaine de rebéllion) n’a pas empêché le gouvernement d’autoriser la construction d’une nouvelle piste d’aéroport. Il faut donc continuer à agir ! Vous pouvez dès maintenant chercher un groupe local près de chez vous et vous inscrire sur www.extinctionrebellion.fr.

### Sources :            
- https://www.extinctionrebellion.fr        
- https://fr.wikipedia.org/wiki/Extinction_Rebellion
- https://www.youtube.com/watch?v=j5PcMoVdTSc&t=49s
- https://www.youtube.com/watch?v=2vUDpdpTVFU
- https://www.podomatic.com/podcasts/xr-podcast/episodes/2019-03-26T06_16_03-07_0
- https://www.reporterre.net/L-Angleterre-declare-l-urgence-climatique-et-relance-l-extension-d-un-aeroport        
- https://www.letemps.ch/opinions/appel-chercheurs-greve-climatique-mondiale-15-mars

Suivre Extinction Rébellion France :

- Site → https://www.extinctionrebellion.fr
- Facebook → https://www.facebook.com/xrfrance/
- Instagram → https://www.instagram.com/extinctionrebellionfrance/
- Twitter → https://www.twitter.com/XtinctionRebel


{{< figure src="dessin3.png" width="70%" >}}

# Principes et valeurs d'Extinction Rébellion

1. Nous partageons une vision du changement (en créant un monde adapté aux générations à venir).
            
2. Nous ajustons notre mission à la mesure de ce qui est nécessaire (en mobilisant 3,5% de la population, seuil à atteindre pour déclencher un changement de système - en utilisant des idées comme celle de « Momentum-driven organizing »).
            
3. Nous avons besoin d’une culture régénératrice (en créant une culture saine, résiliente et adaptable).
            
4. Nous nous remettons nous-mêmes en question, autant que ce système  toxique (en sortant de nos zones de confort pour devenir les acteurs du changement).
            
5. Nous valorisons la réflexion et l’apprentissage (en suivant des cycles d’action, de réflexion, d’apprentissage, puis de planification pour de nouvelles actions. En apprenant des autres mouvements et contextes aussi bien que de nos propres expériences).
            
6. Nous accueillons chaque personne, et chacune de ses facettes (en travaillant activement pour créer des espaces sécurisants et inclusifs).
            
7. Nous limitons délibérément les apports de pouvoir (en démantelant les hiérarchies de pouvoir pour une participation plus équitable).
            
8. Nous ne tenons pas de discours moralisateurs ni culpabilisants (nous vivons dans un système toxique, mais nul ne doit être accusé en tant qu’individu).

9. Nous sommes un réseau non-violent (en utilisant une stratégie et des tactiques non-violentes comme moyen le plus efficace de provoquer le changement).
            
10. Notre mouvement est fondé sur des principes d’autonomie et de décentralisation (nous créons collectivement les structures nécessaires pour défier le pouvoir).Toute personne qui suit ces principes et valeurs essentiels peut agir au nom d’Extinction Rebellion.