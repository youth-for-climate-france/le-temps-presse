---
title: "L'Europe sera-t-elle climatique ou climat'hic ?"
date: 2019-07-07T23:10:15+02:00
draft: false
order: 3
---

Les élections européennes ont eu lieu le week-end du 25-26 mai dans un contexte d’alerte sur le réchauffement climatique et de ses conséquences. Quels étaient ses enjeux ? Quels sont ses résultats ? Qu’est-ce que l’on peut en tirer ? L’Europe sera-t-elle climatique ou climat’hic ?

Si l’Union Européenne n’émet que 10% des gaz à effet de serre mondiaux et que ses émissions baissent depuis les années 1990, le scrutin du 26 mai était l’occasion d’observer la dynamique écologiste en Europe mais aussi si les politiques libérales menées depuis des années allaient être remises en cause. Ces élections, les seules au suffrage universel direct concernant l’UE, étaient amenées à renouveler le parlement européen pour les 5 ans à venir. Parlement qui vote et peut amender les lois mais n’a pas de pouvoir législatif (détenu par la commission, la branche exécutive qui élabore des propositions de lois et gère les politiques de l’UE ). 

## Une percée des libéraux et de l’extrême droite malgré l’essor des verts

Les résultats font état d’une forte montée de l’extrême droite et des libéraux. Les verts gagnent 2,5 points par rapport au scrutin de 2019 et atteignent de ce fait un niveau jamais obtenu. Les deux principaux groupes que sont le PPE (dont est issu Jean-Claude Juncker, actuel président de la commission européenne) et le S&D (sociaux-démocrates) perdent de nom-breux sièges témoignant d’un désamour pour les partis politiques historiques.

En France, la surprise est venue d’Europe Écologie Les Verts (EELV). En effet, seulement crédité de 9% dans les sondages, le parti emmené par Yannick Jadot a réalisé un score de 13,5% (toutefois moindre qu’en 2009), s’affirmant comme le 3ème parti de France. Cet écart est attribué à une mobilisation plus importante que prévue des populations les plus jeunes.

{{< figure src="parlement.jpg" width="60%" >}}

Au niveau européen, le vote écologique a été très présent dans les pays où le niveau de vie moyen est le plus élevé. En effet dans les 11 pays où le niveau de vie annuel moyen est le plus élevé (Allemagne, Autriche, Belgique, Danemark, Finlande, France, Irlande, Luxembourg, Pays-Bas, Royaume-Uni, Suède) les partis « verts » ont réalisé des scores supérieurs à 10%.

L’essor des verts se conjugue avec un recul de la gauche radicale, cela confirme que l’écologie est une préoccupation de gauche et doit être dissociée d’une doctrine libérale. En effet il faut souligner le fait que la majorité des électeurs habituels des partis libéraux et conservateurs de la droite et du centre ne se sont pas orientés vers les partis verts. Ainsi le dogme libéral est toujours la norme chez une grande partie des européens. Pourtant ce n’est pas celui-ci qui permettra d’atténuer le réchauffement climatique.

## Une élection qui confirme les fractures territoriales 

Si l’on fait une analyse géographique du vote en France, les métropoles urbaines dynamiques sont surreprésentées dans le vote EELV.

{{< figure src="votefrance.jpg" width="70%" >}}

EELV a en effet obtenu ses meilleurs résultats dans les métropoles urbaines dynamiques et plus particulièrement dans leurs centres-villes. On peut notamment citer Grenoble, Nantes, Rennes, Toulouse, Montpellier ainsi que Lyon et Paris. Ce constat peut être élargi à l’ensemble de l’Europe occidentale. Les bons résultats de EELV sont généralement corrélés à des bons résultats de La République en Marche. EELV remplace dans ce cas le Rassemblement National, cela dépend généralement du type de populations qui vote. C’est dans les zones périphériques que le RN réalise ses meilleurs scores. Cela témoigne de fortes fractures territoriales.

## Des fractures sociales et générationnelles

En France, si l’on s’intéresse de plus près aux profils des électeurs selon leur vote, on remarque tout d’abord une nette différence entre les votants du RN et de LREM, cela confirme les fractures sociales qui tendent à s’accentuer, on constate aussi qu’une partie de la population est vainqueur de la mondialisation, de la tertiarisation et une autre partie, victime de celles-ci. En effet un ouvrier sur deux (47% ) et près d’un employé sur trois (32%) se sont portés sur le parti mené par Jordan Bardella. On observe aussi cette dualité dans l’abstention : Si la participation a été forte chez les personnes âgées, les plus aisées et plus diplômées de la population, l’abstention (49%) reste importante chez les jeunes de moins de 25 ans (73%) et dans les catégories populaires (57%). En outre LREM n’a réussi qu’à conserver 60% de son électorat par rapport à 2017, 1/5ème ayant voté pour la liste EELV. C’est grâce à 27% des électeurs de Fillon qu’ils ont réussi à obtenir un score honorable. On constate de ce fait un déplacement du parti d’Emmanuel Macron vers la droite. Par ailleurs, les partis de la gauche radicale (France Insoumise et Générations) ont connu un glissement de leurs électeurs vers la liste EELV (17% des électeurs de Jean-Luc Mélenchon, 26% des électeurs de Benoît Hamon) . Cette dernière est arrivée première chez les moins de 35 ans, témoignant d’une prise de conscience chez les plus jeunes grâce notamment aux marches pour le climat. Toutefois c’est parmi les personnes les plus diplômées que EELV a eu ses meilleurs scores alors que les personnes les plus âgées s’orientent davantage vers les partis conservateurs et libéraux (LREM, Les Républicains, RN). On remarque une véritable fracture générationnelle, plus les personnes sont jeunes plus elles votent à gauche, plus elles sont âgées plus elles votent à droite.


{{< figure src="tranchedage.png" width="70%" >}}

Le seul parti mobilisant réellement son électorat habituel est le Rassemblement National (85% des électeurs de Marine Le Pen ). Le résultat de ce parti se présentant comme « antisystème » et comme seule alternative à Emmanuel Macron montre une forte contestation du pouvoir et des politiques libérales mises en places depuis des années au sein de la France et de l’Union Européenne. Par ailleurs, parmi les abstentionnistes, 49% ne sont pas allés voter car ils pensaient que ces élections ne changeraient rien à leurs situations. Les autres raisons sont une contestation à l’égard des partis politiques ou de l’Union Européenne. Beaucoup de monde ne croit plus en les politiques pour résoudre les différents problèmes de société, pourtant c’est eux qui décident, votent les lois et peuvent contraindre les industries polluantes. Si les abstentionnistes étaient allés voter et selon leur conviction, le résultat aurait probablement été différent, LREM n’aurait certainement pas fini 2ème de ces élections (les catégories sociales les plus aisées et les personnes les plus âgées ne s’abstenant pas) , la gauche et les verts auraient pu occuper une place plus importante au parlement. Ce que l’on peut redouter et ce qui est souhaité par Emmanuel Macron est un duel entre « progressistes » et « nationalistes » aux municipales, aux présidentielles, aux législatives. Se profile alors la nécessité pour la gauche de créer une liste commune pour qu’enfin de vraies décisions sociales et écologiques soient prises. 

## Les responsables politiques feront-ils des choix fermes concernant l’urgence climatique ? 

Même si l’on peut se réjouir du bon score d’EELV, il ne faut cependant pas oublier que quelques anciennes icônes de ce parti sont actuellement chez LREM (François De Rugy, Pascal Canfin, Daniel Cohn-Bendit), on peut alors s’interroger sur la ligne politique du parti mais surtout sur sa composition : ses membres sont-ils tous vraiment écologistes ? Ou sont-ils là par simple opportunité de carrière ?

D’autres listes à sensibilité écologique s’étaient présentées, Urgence écologie (1,8%), le parti animaliste (2,2% ), ou encore décroissance 2019 (0,1%). Elles ont permis aux gens de voter selon leurs convictions mais aussi de faire passer des idées nouvelles en se détachant des partis politiques traditionnels.

L’urgence écologique, sociale et climatique semble aujourd’hui être devenue une priorité chez les jeunes Européens, reste à savoir si celle-ci sera réellement prise en compte au sein des institutions de l’Union Européenne, la percée des verts nous laisse entrevoir un espoir. Toutefois les partis libéraux et conservateurs gardant une large majorité, il paraît compliqué de voir de véritables mesures adoptées sur l’atténuation et l’adaptation au réchauffement climatique. Le changement devra se faire par la mobilisation citoyenne. La veille juridique est notamment une solution, elle permet de solliciter ses représentants politiques sur les lois débattues et votées au parlement. En France, les municipales de 2020 seront également importantes. Les respon­sables politiques prendront-ils conscience de l’urgence ou continueront-ils à prôner les « petits pas » ? 

### Sources :

- https://www.ifop.com/publication/europeennes-2019-profil-des-electeurs-et-clefs-du-scrutin/
- https://www.touteleurope.eu/actualite/l-union-europeenne-comment-ca-marche.html
- http://www.europarl.europa.eu/factsheets/fr/chapter/209/le-fonctionnement-de-l-union-europeenne
- https://www.lemonde.fr/politique/article/2019/06/01/vote-aux-europeennes-la-fracture-territoriale-se-creuse_5470247_823448.html
- https://lvsl.fr/le-vote-vert-peut-il-aller-au-dela-des-gagnants-de-la-mondialisation
- https://www.boursorama.com/actualite-economique/actualites/apres-leur-percee-aux-europeennes-les-verts-a-l-heure-des-choix-537baa7f7e0b0666c16b00b1a60290ec
- https://www.insee.fr/fr/statistiques/fichier/3197285/FPORSOC17p4_F6.4.pdf
- https://www.rtbf.be/info/societe/detail_une-vague-verte-europeenne-face-a-l-urgence-climatique?id=10231175
- https://www.francetvinfo.fr/elections/resultats/europe
- https://www.euractiv.fr/section/elections/news/participation-vote-ecolo-vote-rn-les-trois-cartes-qui-expliquent-le-scrutin-en-france/
- http://www.lefigaro.fr/elections/europeennes/europeennes-2019-les-resultats-detailles-par-parti-20190526
- https://www.ipsos.com/fr-fr/europeennes-2019-sociologie-des-electorats— Climat : vers 4 à 5 degrés de plus à la fin du siècle à Paris, Le Monde
- _Comment tout va s’effondrer_, Julien Wosnitza