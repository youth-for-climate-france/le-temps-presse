---
title: "Appel"
date: 2019-07-07T23:02:32+02:00
draft: false
order: 2
---

_Poème écrit par une lycéenne engagée_

L’arbre tombe<br/>
Ses branches crissent craquent crient<br/>
Et le fracas cristallise au milieu du silence<br/>
Il portait le ciel<br/>
Son feuillage touchait les étoiles cachait un merveilleux jardin<br/>
Mais à présent<br/>
L’arbre gît pâle sur le sol<br/>

Ceux qu’il protégeait<br/>
Portent le crime sur leurs fronts<br/>
Leurs mains sont sales de la sève<br/>
Ils sont responsables<br/>
L’enfant caché par les herbes et les arbrisseaux<br/>
Entouré de l’atmosphère musquée<br/>
De mille bruissements pépiements glissements sifflements<br/>
Est témoin<br/>
Il a vu la scène il voit les Hommes se frotter les mains<br/>
Il sait<br/>

{{< figure src="appel1.png" width="40%" >}}

<blockquote>
Ils ont détruit tous ceux qui nous sauvaient<br/>
Arbres animaux plantes<br/>
Je leur ai dit<br/>
Je leur ai dit mais ils ont ri<br/>
Notre monde s’éteint<br/>
Mais ils rient<br/>
Mon monde se meurt je ne peux le regarder<br/>
Disparaître
</blockquote>

Les autres enfants alors le rejoignent<br/>
Une étincelle illumine leurs prunelles

<blockquote>
Tous ensemble<br/>
Nous aurons du pouvoir<br/>
Tous ensemble ils devront nous écouter<br/>
Tous nous devons agir<br/>
Ensemble nous allons changer les choses<br/>
<br/>
Nous n’avons pas le choix pas le temps pas l’expérience <br/>
Nous sommes démunis<br/>
Mais nous marchons<br/>
Réfléchissons<br/>
Ensemble est un si joli mot<br/>
Mais si nous laissons faire qui nous protègera<br/>
<br/>
Toi ?
</blockquote>

{{< figure src="appel2.png" width="80%" >}}