---
title: "Éditorial"
date: 2019-06-26T23:21:53+02:00
order: 1
draft: false
---

Bonjour à vous, militants et militantes pour le climat !

Voilà un deuxième numéro de terminé. Sachez que tous les membres de l'équipe journal se sont énormément investis durant ce mois de juin pour vous offrir un numéro de qualité. Nous espérons qu'il vous plaira et attendons avec impatience vos retours et suggestions. 

Ce journal a été concocté bénévolement par des jeunes de Youth For Climate France, mais ne représente en aucun cas l’opinion générale et nationale du mouvement. Les équipes de rédaction, de graphisme, de mise en page et d’informatique ont travaillé d’arrache-pied pour vous offrir ce contenu. Nous ne sommes pas des journalistes professionnels, mais seulement des collégiens, lycéens et étudiants voulant sensibiliser un maximum de personnes à la cause climatique, et ayant la volonté de faire bouger les choses. 

Des remarques à nous faire ? Des avis à nous donner ? Des contributions à proposer ? Ou simplement le besoin ou l'envie de nous contacter ? Envoyez-nous un e-mail à l’adresse suivante :

<letempspresse@protonmail.com>

Sur ce, nous vous laissons profiter de votre journal, et n’oubliez jamais : le temps presse. Bonne lecture !

{{< author "L’équipe journal de Youth for Climate France" >}}