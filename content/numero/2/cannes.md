---
title: "Youth for Climate au festival de Cannes"
date: 2019-07-07T23:48:13+02:00
draft: false
order: 8
---

Le 16 et 17 mai 2019, différents membres de plusieurs mouvements climat se sont rejoints à Cannes pour monter les marches du festival. Ainsi, Adélaïde et Louise de Youth For Climate Belgique, Camille de On Est Prêts, Benoît de Youth For Climate France ainsi que Vipulan de Little Citizens For Climate et Youth For Climate France ont pu parler de la cause climatique lors de la montée des marches le jeudi 16 mai 2019, et lors d’une conférence de presse organisée le 17 mai 2019.

Il a été possible pour tous ces activistes de monter les marches grâce aux contacts du groupe On Est Prêts. Vipulan nous explique qu’il s’agit d’une très belle opportunité car « c’est un événement très médiatique » puis nous déclare que « la montée des marches était symbolique, mais [que] la conférence de presse était plus utile pour faire passer le message ». Cette conférence de presse a été organisée suite à la tribune publiée par Cyril Dion et signée par plus de 200 célébrités.

Vipulan nous explique ensuite que le but était « d’utiliser la très grande médiatisation de l’événement pour faire passer le message climatique ; car le monde du cinéma peut nous aider ».

« Les célébrités sont des humains, et beaucoup ne sont pas des oppor­tunistes » a ensuite déclaré le jeune garçon, en citant l’exemple de la comédienne Lucie Lucas, qui s’engage réellement dans une ferme de perma­culture par exemple.

Ce petit regroupement a également permis, d’après Vipulan, d’apprendre à mieux connaître Youth For Climate Belgique, et de renforcer le lien international.

Il nous dit enfin que si l’expérience était à refaire, il faudrait s’y prendre plus à l’avance pour pouvoir être entendu par de plus nombreuses personnes, pour faire venir des activistes d’autres pays, ce qui permettrait une plus grande diffusion du message.
